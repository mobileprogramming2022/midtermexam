import 'dart:io';
import 'dart:math';

void main(List<String> arguments) {
  print("Input the expression: ");
  String tokens = stdin.readLineSync()!;
  print("infix : ");
  print(isString(tokens));
  print("Postfix : ");
  print(In_to_Post(isString(tokens)));
  print("Evaluate Postfix is: ");
  print(EvaluatePost(In_to_Post(isString(tokens))));
}

List isString(String input) {
  List<String> list = [];
  String temp = "";
  for (var i = 0; i < input.length; i++) {
    if (input[i] == " ") {
      if (temp.isNotEmpty) {
        list.add(temp);
        temp = "";
      }
    } else if (input[i] == "") {
      if (temp.isNotEmpty) {
        list.add(temp);
        temp = "";
      }
    } else if (input[i] == "*" ||
        input[i] == "/" ||
        input[i] == "^" ||
        input[i] == "(" ||
        input[i] == ")" ||
        input[i] == "+" ||
        input[i] == "-") {
      if (temp.isNotEmpty) {
        list.add(temp);
        temp = "";
      }
      list.add(input[i]);
    } else {
      temp = temp + input[i];
    }
  }
  if (!input[input.length - 1].contains(" ")) {
    list.add(temp);
  }
  return list;
}

bool isNumber(String x) {
  for (int i = 0; i < 10; i++) {
    if (x[0] == i.toString()) {
      return true;
    }
  }
  return false;
}

isOperator(String a1) {
  switch (a1) {
    case '+':
    case '-':
      return 1;
    case '*':
    case '/':
      return 2;
    case '^':
      return 3;
  }
  return -1;
}

List In_to_Post(List list) {
  var Postfix = [];
  var Operator = [];
  for (var i = 0; i < list.length; i++) {
    if (isNumber(list[i])) {
      Postfix.add(list[i]);
    }
    if (list[i] == "+" ||
        list[i] == "-" ||
        list[i] == "*" ||
        list[i] == "/" ||
        list[i] == "^") {
      while (Operator.isNotEmpty &&
          Operator[Operator.length - 1] != "(" &&
          isOperator(list[i]) < isOperator(Operator[Operator.length - 1])) {
        Postfix.add(Operator[Operator.length - 1]);
        Operator.removeAt(Operator.length - 1);
      }
      Operator.add(list[i]);
    }
    if (list[i] == "(") {
      Operator.add(list[i]);
    }
    if (list[i] == ")") {
      while (Operator[Operator.length - 1] != "(") {
        Postfix.add(Operator[Operator.length - 1]);
        Operator.removeAt(Operator.length - 1);
      }
      Operator.removeAt(Operator.length - 1);
    }
  }
  while (Operator.isNotEmpty) {
    Postfix.add(Operator[Operator.length - 1]);
    Operator.removeAt(Operator.length - 1);
  }
  return Postfix;
}

List EvaluatePost(List Postfix) {
  var keepNum = [];
  for (var i = 0; i < Postfix.length; i++) {
    if (isNumber(Postfix[i])) {
      keepNum.add(Postfix[i]);
    } else {
      var a = keepNum[keepNum.length - 1];
      var b = keepNum[0];
      int right = int.parse(a);
      int left = int.parse(b);
      keepNum.removeAt(keepNum.length - 1);
      keepNum.removeAt(0);

      switch (Postfix[i]) {
        case '+':
          keepNum.add((left + right).toString());
          break;
        case '-':
          keepNum.add((left - right).toString());
          break;
        case '*':
          keepNum.add((left * right).toString());
          break;
        case '/':
          keepNum.add((left / right).toString());
          break;
        case '^':
          keepNum.add(pow(left, right).toString());
          break;
      }
    }
  }
  return keepNum;
}
